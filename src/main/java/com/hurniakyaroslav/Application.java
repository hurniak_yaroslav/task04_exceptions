package com.hurniakyaroslav;

import com.hurniakyaroslav.view.ConsoleView;

public class Application {
    public static void main(String[] args) {
        new ConsoleView().show();
    }
}
