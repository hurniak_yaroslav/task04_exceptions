package com.hurniakyaroslav.view;

@FunctionalInterface
public interface Printable {
    void print();
}
