package com.hurniakyaroslav;

public class Flower {

    FlowerType type;

    int price;

    Flower() {
        price = 0;
    }

    public Flower( FlowerType type) {
        this.type = type;
        this.price = type.getPrice();
    }


    public int getPrice() {
        return price;
    }

    public FlowerType getType() {
        return type;
    }
}
