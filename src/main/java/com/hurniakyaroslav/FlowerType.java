package com.hurniakyaroslav;

public enum FlowerType {
    ROSE{
       public int price = 20;

        @Override
        public int getPrice() {
            return price;
        }
    }, CHAMOMILE{
        int price = 3;

        @Override
        public int getPrice() {
            return price;
        }
    }, VIOLET{
        int price = 7;

//        @Override
//        public int getPrice() {
//            return price;
//        }
    };

    public int getPrice(){
             throw new RuntimeException("Can`t find getPrice() method of this flower`s type!");
        }

    }
