package com.hurniakyaroslav.view;

public class UnavailableFlower extends Exception{
    UnavailableFlower(){
        super("Sorry, but this flower is unavailable. Please select another.");
    }
}
